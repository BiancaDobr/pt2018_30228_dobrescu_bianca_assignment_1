package PT2018.demo.DemoProject1;
import java.util.ArrayList;
import java.util.List;

public class Polinom
{
  private List<Monom> sirMonoame = new ArrayList<Monom>();
  private int grad;
  
  public Polinom() {

	}
  
  public Polinom(double[] p)
  {
      int temp = 0;
	  while (temp <p.length)
	  {
      Monom a = new Monom(p.length - temp - 1, p[temp]);
      sirMonoame.add(a);
      this.grad = (p.length - 1);
      temp++;
    }
  }
  
  public Polinom multPol(Polinom p) {
	   List<Monom> pol_unu = new ArrayList<Monom>();
	    List<Monom> pol_doi = new ArrayList<Monom>();
	      pol_unu = this.sirMonoame;
	      pol_doi = p.sirMonoame;
	      
	    Polinom rezultat = new Polinom();
	   // Polinom aux = new Polinom();
	    rezultat.grad = this.grad + p.grad;
	   // aux.grad = this.grad + p.grad;
	    
	    for (Monom a : pol_unu) {
	        for (Monom b : pol_doi)
	        {
	            rezultat.sirMonoame.add(a.multMon(b));
	    }
	    }
	    
	    
	   // for (Monom l : aux) {
	     //   for (Monom q : aux)
	       // {
	        //	if (l.grad == q.grad) {
	          //      rezultat.sirMonoame.add(q.adunMon(q));
	            //  }
	    //}
	    //}
	   return rezultat;
}
  
  public Polinom adunPol(Polinom p)
  {
    List<Monom> pol_unu = new ArrayList<Monom>();
    List<Monom> pol_doi = new ArrayList<Monom>();
      pol_unu = this.sirMonoame;
      pol_doi = p.sirMonoame;
      
    Polinom rezultat = new Polinom();
    
    for (Monom a : pol_unu) {
      for (Monom b : pol_doi)
      {
        if (a.grad == b.grad) {
          rezultat.sirMonoame.add(a.adunMon(b));
        }
      }
  }
    return rezultat;
 }
  

  public Polinom subsPol(Polinom p)
 {
	    List<Monom> pol_1 = new ArrayList<Monom>();
	    List<Monom> pol_2 = new ArrayList<Monom>();
	      pol_1 = this.sirMonoame;
	      pol_2 = p.sirMonoame;
	      
	    Polinom rez = new Polinom();
	    
	    for (Monom c : pol_1) {
	      for (Monom d : pol_2)
	      {
	        if (c.grad == d.grad) {
	          rez.sirMonoame.add(c.subsMon(d));
	        }
	      }
	  }
	    return rez;
}
		  
  
  public Polinom derivPol(Polinom p) {
    Polinom rezultat = new Polinom();
    for (Monom m : sirMonoame)
      rezultat.sirMonoame.add(m.derivMon(m));
    return rezultat;
  }
  
  public Polinom integrPol(Polinom p)
  {
    Polinom rezultat = new Polinom();
    for (Monom m : sirMonoame)
      rezultat.sirMonoame.add(m.integrMon(m));
    return rezultat;
  }
  
  
  public String toString() {
    String s = "";
    for (Monom m : sirMonoame) {
      if (((Monom)m).getCoef() != 0) {
        s = s + "(" + ((Monom)m).toString() + ")" + "+";
      }
    }
    if (s != null) {
      s = s.substring(0, s.length() - 1);
    }
    return s;
  }
}