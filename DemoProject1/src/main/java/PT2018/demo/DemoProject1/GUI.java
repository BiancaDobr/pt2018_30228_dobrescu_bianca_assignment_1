package PT2018.demo.DemoProject1;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



public class GUI extends JFrame
{
  private JFrame frame;
  private JTextField textInsPol1;
  private JTextField textInsPol2;
  private JLabel lblP1;
  private JLabel lblP2;
  private JLabel labelError;
  private JLabel arataAdunare;
  private JLabel arataScadere;
  private JLabel arataInmultire;
  private JLabel arataDerivare;
  private JLabel arataIntegrare;
  private JButton butonAdunarePol;
  private JButton butonScaderePol;
  private JButton butonInmultirePol;
  private JButton butonDerivarePol;
  private JButton butonIntegrarePol;
  private Polinom p1 = new Polinom();
  private Polinom p2 = new Polinom();
  
  public GUI()
  {
    frame = new JFrame();
    
    frame.setDefaultCloseOperation(3);
    frame.setTitle("Procesarea Polinoamelor");
    frame.setSize(1200, 800);
    frame.setVisible(true);
    frame.setLayout(null);
    setLocationRelativeTo(null);
    
    textInsPol1 = new JTextField();
    textInsPol1.setBounds(70, 40, 400, 40);
    textInsPol1.setLayout(null);
    textInsPol1.setVisible(true);
    textInsPol1.setColumns(200);
  
    
    textInsPol2 = new JTextField();
    textInsPol2.setBounds(580, 40, 400, 40);
    textInsPol2.setLayout(null);
    textInsPol2.setVisible(true);
    textInsPol2.setColumns(200);
    textInsPol2.setColumns(90);
    
    frame.add(textInsPol1);
    frame.add(textInsPol2);
    
    arataAdunare = new JLabel("Rezultat adunare: ");
    arataScadere = new JLabel("Rezultat scadere: ");
    arataInmultire = new JLabel("Rezultat inmultire: ");
    arataDerivare = new JLabel("Rezultat derivare: ");
    arataIntegrare = new JLabel("Rezultat integrare: ");
    
    
    lblP1 = new JLabel("P1");
    lblP2 = new JLabel("P2");
    labelError = new JLabel(" ");
    
    frame.add(arataAdunare);
    frame.add(arataScadere);
    frame.add(arataInmultire);
    frame.add(arataDerivare);
    frame.add(arataIntegrare);
    frame.add(lblP1);
    frame.add(lblP2);
    frame.add(labelError);
    
    labelError.setBounds(600, 800, 400, 300);
    labelError.setVisible(true);
    
    lblP1.setBounds(40, 35, 400, 50);
    lblP1.setVisible(true);
    
    lblP2.setBounds(550, 35, 400, 50);
    lblP2.setVisible(true);
    
    arataAdunare.setBounds(600, 25, 400, 300);
    arataAdunare.setVisible(true);
    
    arataScadere.setBounds(600, 125, 400, 300);
    arataScadere.setVisible(true);
    
    arataInmultire.setBounds(600, 225, 400, 300);
    arataInmultire.setVisible(true);
   
    
    arataDerivare.setBounds(600, 325, 400, 300);
    arataDerivare.setVisible(true);
    
    arataIntegrare.setBounds(600, 425, 400, 300);
    arataIntegrare.setVisible(true);
    
    butonAdunarePol = new JButton("Aduna polinomul P1 si P2");
    frame.add(butonAdunarePol);
    butonAdunarePol.setBounds(200, 150, 200, 40);
    
    butonScaderePol = new JButton("Scade polinomul P1 din P2");
    butonScaderePol.setBounds(200, 250, 200, 40);
    frame.add(butonScaderePol);
    
    butonInmultirePol = new JButton("Inmulteste pol. P1 cu P2");
    frame.add(butonInmultirePol);
    butonInmultirePol.setBounds(200, 350, 200, 40);
    
    butonDerivarePol = new JButton("Deriveaza polinomul P1");
    frame.add(butonDerivarePol);
    butonDerivarePol.setBounds(200, 450, 200, 40);
    
    butonIntegrarePol = new JButton("Integreaza polinomul P1");
    frame.add(butonIntegrarePol);
    butonIntegrarePol.setBounds(200, 550, 200, 40);
    
    butonAdunarePol.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			String s1, s2;
			double[] a1;
			double[] a2;
			String[] noOfCoeff1;
			String[] noOfCoeff2;
			int i;

			s1 = textInsPol1.getText();
			s2 = textInsPol2.getText();

			noOfCoeff1 = s1.split(",");
			noOfCoeff2 = s2.split(",");
			a1 = new double[noOfCoeff1.length];
			a2 = new double[noOfCoeff2.length];

			for (i = 0; i < noOfCoeff1.length; i++) {
				a1[i] = Double.parseDouble(noOfCoeff1[i]);
			}

			for (i = 0; i < noOfCoeff2.length; i++) {
				a2[i] = Double.parseDouble(noOfCoeff2[i]);
			}

			p1 = new Polinom(a1);
			p2 = new Polinom(a2);
			arataAdunare.setText(p1.adunPol(p2).toString());
		}
	});

    butonScaderePol.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			String s1, s2;
			double[] a1;
			double[] a2;
			String[] noOfCoeff1;
			String[] noOfCoeff2;
			int i;

			s1 = textInsPol1.getText();
			s2 = textInsPol2.getText();

			noOfCoeff1 = s1.split(",");
			noOfCoeff2 = s2.split(",");
			a1 = new double[noOfCoeff1.length];
			a2 = new double[noOfCoeff2.length];

			for (i = 0; i < noOfCoeff1.length; i++) {
				a1[i] = Double.parseDouble(noOfCoeff1[i]);
				}

				for (i = 0; i < noOfCoeff2.length; i++) {
					a2[i] = Double.parseDouble(noOfCoeff2[i]);
				}

				p1 = new Polinom(a1);
				p2 = new Polinom(a2);
				arataScadere.setText(p1.subsPol(p2).toString());
			}
		
	});

    butonInmultirePol.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			String s1, s2;
			double[] a1;
			double[] a2;
			String[] noOfCoeff1;
			String[] noOfCoeff2;
			int i;

			s1 = textInsPol1.getText();
			s2 = textInsPol2.getText();

			noOfCoeff1 = s1.split(",");
			noOfCoeff2 = s2.split(",");
			a1 = new double[noOfCoeff1.length];
			a2 = new double[noOfCoeff2.length];

			for (i = 0; i < noOfCoeff1.length; i++) {
				a1[i] = Double.parseDouble(noOfCoeff1[i]);
			}

			for (i = 0; i < noOfCoeff2.length; i++) {
				a2[i] = Double.parseDouble(noOfCoeff2[i]);
			}

			p1 = new Polinom(a1);
			p2 = new Polinom(a2);
			arataInmultire.setText(p1.multPol(p2).toString());

		}
	});

    butonDerivarePol.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			String s1;
			double[] a1;
			String[] noOfCoeff1;
			int i;

			s1 = textInsPol1.getText();
			noOfCoeff1 = s1.split(",");
			a1 = new double[noOfCoeff1.length];

			for (i = 0; i < noOfCoeff1.length; i++) {
				a1[i] = Double.parseDouble(noOfCoeff1[i]);
			}

			p1 = new Polinom(a1);

			arataDerivare.setText(p1.derivPol(p1).toString());

		}
	});

    butonIntegrarePol.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent arg0) {
			String s1;
			double[] a1;
			String[] noOfCoeff1;
			s1 = textInsPol1.getText();
			int i;
			
			noOfCoeff1 = s1.split(",");
			a1 = new double[noOfCoeff1.length];

			for (i = 0; i < noOfCoeff1.length; i++) {
				a1[i] = Double.parseDouble(noOfCoeff1[i]);
			}

			p1 = new Polinom(a1);
			arataIntegrare.setText(p1.integrPol(p1).toString());

		}
	});
}
  
}