package PT2018.demo.DemoProject1;
public class Monom
{
  protected int grad;
  protected Double coef;

  public Double getCoef() {
    return coef;
  }

  public Monom(int grad, Double coef) {
    this.grad = grad;
    this.coef = coef;
  }

  public Monom adunMon(Monom m) {
      return new Monom(grad, this.coef + m.coef);
  }
  
  public Monom subsMon(Monom m) {
	      return new Monom(grad, this.coef - m.coef);
	  }
  
  public Monom multMon(Monom m)
  {
    return new Monom(this.grad + m.grad, this.coef * m.coef);
  }

  
  public Monom integrMon(Monom m) 
  {
	  if (grad >= 0) {
	      return new Monom(grad + 1, coef * 1 / (grad + 1));
	    }
	  return new Monom(0, Double.valueOf(0.0D));
  }
  
  
  public Monom derivMon(Monom m)
  {
    if (grad - 1 >= 0) {
      return new Monom(grad - 1, coef * grad);
    }
    return new Monom(0, Double.valueOf(0.0D));
  }

  public String toString()
  {
    String s = "";
    s = s + coef + "x^" + grad;
    return s;
  }
}
